>./env/.env_third
for var in $(compgen -v | grep -Ev '^(BASH)'); do
    var_fixed=$(printf "%s" "${!var}" | tr -d '\n' )
    echo "$var=${var_fixed}" >>./env/.env
done