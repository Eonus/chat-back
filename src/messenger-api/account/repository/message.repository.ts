import { EntityRepository, Repository } from 'typeorm';
import { SourceAccountEntity } from '../entity/account.entity';

@EntityRepository(SourceAccountEntity)
export class SourceAccountRepository extends Repository<SourceAccountEntity> {}
