import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SourceAccountRepository } from './repository/message.repository';
import { SourceAccountService } from './service/account.service';

@Module({
  imports: [TypeOrmModule.forFeature([SourceAccountRepository])],
  providers: [SourceAccountService],
  exports: [SourceAccountService],
})
export class SourceAccountModule {}
