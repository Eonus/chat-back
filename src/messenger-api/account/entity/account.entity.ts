import { Column, Entity, Index, PrimaryColumn } from 'typeorm';
import { SourceAccountI } from '../types';

export const SOURCE_ACCOUNT_TABLE_NAME = 'source_accounts';

@Entity(SOURCE_ACCOUNT_TABLE_NAME)
export class SourceAccountEntity implements SourceAccountI {
  @Index({ unique: true })
  @PrimaryColumn({ type: 'text', nullable: false, unique: true })
  uuid: string;
  @Column()
  @Index()
  source: string;
  @Column()
  login: string;
  @Column()
  owner: string;
}
