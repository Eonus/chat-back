export type SourceAccountI = {
  uuid: string;
  source: string;
  login: string;
  owner: string;
};
