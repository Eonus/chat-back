import { Injectable, NotFoundException } from '@nestjs/common';
import { SourceAccountRepository } from '../repository/message.repository';
import { SourceAccountI } from '../types';

@Injectable()
export class SourceAccountService {
  constructor(private readonly repository: SourceAccountRepository) {}
  create = async (account: SourceAccountI) => {
    return await this.repository.save(account);
  };
  getAccountBySourceAndLogin = async (source: string, login: string) => {
    const account = await this.repository.findOne({ source, login });
    if (!account) throw new NotFoundException();
    return account;
  };
}
