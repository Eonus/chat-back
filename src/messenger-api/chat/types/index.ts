export type SourceChatI = {
  thread: string;
  source: string;
};
