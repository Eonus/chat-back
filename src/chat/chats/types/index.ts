import { SourceAccountI } from 'src/messenger-api/account/types';
import { SourceChatI } from 'src/messenger-api/chat/types';

export enum ChatType {
  Channel = 1,
  Chat = 2,
}

export type ChatId = string;

export interface ChatI {
  uuid: ChatId;
  title: string;
  type: ChatType;
  description?: string;
  sourceAccount?: SourceAccountI;
  sourceChat?: SourceChatI;
}
