import { Module } from '@nestjs/common';
import { ChatsModule } from './chats/chats.module';
import { MessagesModule } from './messages/messages.module';

@Module({
  imports: [MessagesModule, ChatsModule],
})
export class ChatMainModule {}
