import { Column, Entity, Index, PrimaryColumn } from 'typeorm';
import { MessageI } from '../types';

export const MESSAGE_TABLE_NAME = 'messages';

@Entity(MESSAGE_TABLE_NAME)
export class MessageEntity implements MessageI {
  @Index({ unique: true })
  @PrimaryColumn({ type: 'text', nullable: false, unique: true })
  uuid: string;
  @Column({ type: 'text', nullable: false })
  from: string;
  @Column({ type: 'text', nullable: false })
  to: string;
  @Column({ type: 'bigint', nullable: false })
  time: number;
  @Column({ type: 'text', nullable: false })
  thread: string;
  @Column({ type: 'text', nullable: false })
  item: string;
  @Column({ type: 'text', nullable: true })
  status?: string;
  @Column({ type: 'boolean', nullable: false })
  incoming: boolean;
  @Column({ type: 'text', nullable: false })
  source: string;
  @Column({ type: 'text', nullable: false })
  chatUuid: string;
  @Column({ type: 'text', nullable: true })
  text: string | null;
  @Column({ type: 'jsonb', nullable: true })
  content: { type: string; src: string }[] | null;
}
