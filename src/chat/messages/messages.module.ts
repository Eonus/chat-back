import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MessageController } from './controller/message.controller';
import { MessageRepository } from './repository/message.repository';
import { MessageService } from './service/message.service';

@Module({
  imports: [TypeOrmModule.forFeature([MessageRepository])],
  providers: [MessageService],
  controllers: [MessageController],
})
export class MessagesModule {}
