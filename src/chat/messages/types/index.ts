import { ChatId } from 'src/chat/chats/types';

export interface BaseMessageI {
  text: string | null;
  content:
    | {
        type: string;
        src: string;
      }[]
    | null;
  uuid?: string;
}
export interface MessageI extends BaseMessageI {
  from: string;
  to: string;
  time: number;
  thread: string;
  item: string;
  status?: string;
  incoming: boolean;
  source: string;
  chatUuid: ChatId;
}

export type MessageFilter = {
  skip?: number;
  limit?: number;
  searchText?: string;
};
