import { Injectable, NotFoundException } from '@nestjs/common';
import { MessageRepository } from '../repository/message.repository';
import { MessageFilter, MessageI } from '../types';
import { FindManyOptions, Like } from 'typeorm';

@Injectable()
export class MessageService {
  constructor(private readonly repository: MessageRepository) {}

  create = async (message: MessageI) => {
    return await this.repository.save(message);
  };

  getMany = async (filter: MessageFilter) => {
    const convertedFilter: FindManyOptions<MessageI> = { take: filter.limit, skip: filter.skip, where: {} };
    if (filter.searchText) {
      convertedFilter.where = {
        message: Like(`%${filter.searchText}%`),
      };
    }
    // TODO: fix text search
    return await this.repository.find(convertedFilter);
  };

  getOne = async (uuid: string) => {
    const message = await this.repository.findOne({ uuid });
    if (!message) throw new NotFoundException();
    return message;
  };
}
