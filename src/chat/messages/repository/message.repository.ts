import { EntityRepository, Repository } from 'typeorm';
import { MessageEntity } from '../entity/message.entity';

@EntityRepository(MessageEntity)
export class MessageRepository extends Repository<MessageEntity> {}
