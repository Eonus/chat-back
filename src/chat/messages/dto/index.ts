import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsOptional, IsUrl, IsUUID, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { BaseMessageI, MessageFilter } from '../types';

class MessageContentDTO {
  @ApiProperty({ required: true })
  type: string;
  @ApiProperty({ required: true })
  @IsUrl()
  src: string;
}

export class BaseMessageDTO implements BaseMessageI {
  @ApiProperty({ required: false })
  @IsOptional()
  text: string;
  @ApiProperty({ required: false, isArray: true, type: MessageContentDTO })
  @IsOptional()
  @ValidateNested({ each: true })
  //   @Transform((content: MessageContentDTO) => content)
  content: MessageContentDTO[];
  @ApiProperty({ required: false })
  @IsOptional()
  @IsUUID()
  uuid?: string;
}

export class GetMessagesDTO implements MessageFilter {
  @ApiProperty({ required: false })
  @IsInt()
  @IsOptional()
  @Type(() => Number)
  limit?: number;

  @ApiProperty({ required: false })
  @IsInt()
  @IsOptional()
  @Type(() => Number)
  skip?: number;

  @ApiProperty({ required: false })
  @IsOptional()
  searchText?: string;
}
