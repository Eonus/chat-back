import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { BaseMessageDTO, GetMessagesDTO } from '../dto';
import { MessageService } from '../service/message.service';
import { MessageI } from '../types';
import { v4 as getUuid } from 'uuid';

@ApiTags('Messages')
@Controller('message')
export class MessageController {
  constructor(private readonly service: MessageService) {}
  @Get('')
  async getMessages(@Query() filter: GetMessagesDTO) {
    return await this.service.getMany(filter);
  }
  @Get(':uuid')
  async getMessage(@Param('uuid') uuid: string) {
    return await this.service.getOne(uuid);
  }
  @Post('')
  @ApiOperation({})
  async createMessage(@Body() baseMessage: BaseMessageDTO) {
    if (!baseMessage.uuid) baseMessage.uuid = getUuid();
    const message: MessageI = {
      ...baseMessage,
      from: 'test',
      time: Date.now(),
      thread: '',
      to: 'test2',
      item: '',
      incoming: false,
      source: 'whatsapp',
      chatUuid: '',
    };
    return await this.service.create(message);
  }
}
