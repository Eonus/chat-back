import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ChatMainModule } from './chat/chat.module';

@Module({
  imports: [TypeOrmModule.forRoot(), ChatMainModule],
})
export class AppModule {}
